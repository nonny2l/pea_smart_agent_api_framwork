﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace RRApiFramework.Model.Enum
{
    public enum StatusCode
    {
        [DefaultValue("EROR")]
        EROR = 101,


        [DefaultValue("Success")]
        OK = 200,

        [DefaultValue("Cannot search data by keywords for edit, please check search keywords.")]
        NoDataForEdit = 300,

        [DefaultValue("Cannot search data by keywords for delete, please check search keywords.")]
        NoDataForDelete = 301,

        [DefaultValue("Cannot save data into database, Invalid data.")]
        InvalidData = 330,

        [DefaultValue("Cannot save data into database.")]
        NotSave = 350,

        [DefaultValue("Cannot save data into database,\n document date has closed.")]
        NotSaveDocumentHasMonthClosed = 360,

        [DefaultValue("NULL parameter.")]
        NullParameter = 600,

        [DefaultValue("Not support file.")]
        NotSupportFile = 630,

        [DefaultValue("Blank file.")]
        BlankFile = 635,

        [DefaultValue("Broken file.")]
        BrokenFile = 640,

        [DefaultValue("Insert Duplicate.")]
        Duplicate = 680,

        [DefaultValue("Cannot Generate auto primary key.")]
        NoPrimaryKey = 700,

        [DefaultValue("Incorrect Password.")]
        PasswordInCorrect = 800,

        [DefaultValue("Incorrect Primary Key.")]
        PrimaryKeyInCorrect = 805,

        [DefaultValue("Document status not match.")]
        DocumentStatusNotMatch = 810,

        [DefaultValue("No Permission.")]
        NoPermission,

        [DefaultValue("Cannot search data by keywords.")]
        NotFoundData = 390,

        [DefaultValue("Has Exception.")]
        Exception = 900
    }
}