﻿using RRApiFramework.Model.Enum;
using RRApiFramework.Security;
using RRApiFramework.Utility;
using RRPlatFormModel;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Transactions;
using System.Web.Configuration;

namespace RRApiFramework.Model
{
    /// <summary>
    /// คอนโทรลเลอร์หลักสำหรับจัดการข้อมูลของระบบ
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class AbRepository<T> where T : class, new()
    {
        public AbRepository()
        {
            var configuration = WebConfigurationManager.OpenWebConfiguration("~");
            var section = (ConnectionStringsSection)configuration.GetSection("connectionStrings");
            string server = EncryptionClass.Decrypt(ConfigurationUtil.Static.ModelConnectionStringServer, ConfigurationUtil.Static.InitCrypto);
            string user = EncryptionClass.Decrypt(ConfigurationUtil.Static.ModelConnectionStringUser, ConfigurationUtil.Static.InitCrypto);
            string pass = EncryptionClass.Decrypt(ConfigurationUtil.Static.ModelConnectionStringPassword, ConfigurationUtil.Static.InitCrypto);
            section.ConnectionStrings[ConfigurationUtil.Static.ModelName].ConnectionString =  server + user + pass ;
            configuration.Save();

            dbContext = new RRSmartAgentModelContext();
            this.data = new T();
            this.datas = new List<T>();
        }
        public RRSmartAgentModelContext dbContext;
        public DbContextTransaction dbContextTransaction;
        private T data;
        private List<T> datas;
        private Type typeData;
        private string moduleName;
        //private static readonly ILog log = log4net.LogManager.GetLogger(typeof(T));
        // private LogManager logManager;


        /// <summary>
        /// <see cref="ModelEntities"/> เพื่อจัดการข้อมูลในฐานข้อมูล
        /// </summary>
        public RRSmartAgentModelContext DbContext
        {
            get { return this.dbContext; }
            set { this.dbContext = value; }
        }

        /// <summary>
        /// <see cref="DbContextTransaction"/> ใช้กรณีต้องบันทึกข้อมูลเป็นชุด
        /// </summary>
        public DbContextTransaction DbContextTransaction
        {
            get { return this.dbContextTransaction; }
            set { this.dbContextTransaction = value; }
        }
        protected string ModuleName
        {
            get { return this.moduleName = "AbRepository"; }
            set { this.moduleName = value; }
        }

        public virtual T Data
        {
            get { return this.data; }
            set { this.data = value; }
        }

        public virtual List<T> Datas
        {
            get { return this.datas; }
            set { this.datas = value; }
        }

        protected Type TypeClass
        {
            get { return typeof(T); }
        }


        public virtual bool Create(T data)
        {
            bool result = false;
            this.Save("0", data, ActionType.Add, out result);
            this.dbContext.SaveChanges();
            //if (result)
            //    this.logManager.SaveChanges();s
            return result;

        }


        public virtual bool Delete(string id, T data)
        {
            bool result = false;
            this.Save(id, data, ActionType.Delete, out result);
            this.dbContext.SaveChanges();
            //if (result)
            //    this.logManager.SaveChanges();
            return result;

        }


        public virtual bool Edit(string id, T data)
        {
            bool result = false;
            this.Save(id, data, ActionType.Edit, out result);
            this.dbContext.SaveChanges();
            //if (result)
            //    this.logManager.SaveChanges();
            return result;

        }

        public virtual void SaveLogTransection(string key, ActionType action)
        {
            /// ยังไม่ทราบ Table ที่ใช้งาน
            StackFrame stackFrame = new StackFrame(1, true);
            string[] temp = stackFrame.GetFileName().Split('\\');
            string fileClassName = temp.Length > 0 ? temp[temp.Length - 1] : temp[0];
            string functionName = stackFrame.GetMethod().Name;
            int fileLineNumber = stackFrame.GetFileLineNumber();
            int fileColumnNumber = stackFrame.GetFileColumnNumber();
            string className = stackFrame.GetMethod().DeclaringType.Name;

        }
        public virtual void SaveLogErorTransection(string key, ActionType action, Exception exception)
        {

            try
            {
                string resultException = GetEntityErrorException(exception);


                var errorInfo = string.Empty;

                StackFrame stackFrame = new StackFrame(1, true);
                string[] temp = stackFrame.GetFileName().Split('\\');
                string fileClassName = temp.Length > 0 ? temp[temp.Length - 1] : temp[0];
                string functionName = stackFrame.GetMethod().Name;
                int fileLineNumber = stackFrame.GetFileLineNumber();
                int fileColumnNumber = stackFrame.GetFileColumnNumber();
                string className = stackFrame.GetMethod().DeclaringType.Name;
                string[] stack = exception.StackTrace.Split('\n');

                errorInfo += "///* Comment *///";
                errorInfo += Environment.NewLine;
                errorInfo += "File Name : " + fileClassName + Environment.NewLine;
                errorInfo += "Class Name : " + className + Environment.NewLine;
                errorInfo += "Method Name : " + functionName + Environment.NewLine;
                errorInfo += "Line Number : " + fileLineNumber + Environment.NewLine;
                errorInfo += "Column Number : " + fileColumnNumber + Environment.NewLine;
                errorInfo += "////////////////";

            }
            catch (Exception ex)
            {
                var err = GetEntityErrorException(ex);
            }
        }


        private static string GetEntityErrorException(Exception exception)
        {
            string resultException = string.Empty;

            var entityException = exception.GetType();
            if (entityException == typeof(DbEntityValidationException))
            {
                resultException += EntityErrorException(exception);
            }
            else
            {
                if (exception.InnerException != null)
                {
                    if (exception.InnerException.InnerException != null)
                    {
                        if (exception.InnerException.InnerException.InnerException != null)
                            resultException += exception.InnerException.InnerException.InnerException.Message + Environment.NewLine;
                        else
                            resultException += exception.InnerException.InnerException.Message + Environment.NewLine;
                    }
                    else
                    {
                        resultException += exception.InnerException.Message + Environment.NewLine;
                    }
                }
                else
                {
                    resultException += exception.Message + Environment.NewLine;
                }
            }
            return resultException;
        }
        private static string EntityErrorException(Exception exception)
        {
            string result = string.Empty;
            var errorResult = string.Empty;
            var counts = 1;
            var entityErrors = (DbEntityValidationException)exception;
            foreach (var error in entityErrors.EntityValidationErrors)
            {
                foreach (var err in error.ValidationErrors)
                {
                    if (errorResult.Length > 0)
                        errorResult += Environment.NewLine;

                    errorResult += string.Format("{0}. Error Message : {2}",
                        counts, err.PropertyName, err.ErrorMessage);
                    counts++;
                }
            }

            return errorResult + Environment.NewLine;
        }

        /// <summary>
        /// การเรียกดูข้อมูลแบบแถวเดียวโดยต้องส่งรหัสของแถว (id or primary key) มาด้วย
        /// </summary>
        /// <param name="id">
        /// id เป็นรหัสของแถวหรือเป็นคีย์หลัก
        /// </param>
        /// <returns>
        /// กรณีพบข้อมูลตามเงื่อนไขที่กำหนด คืนค่าข้อมูลแบบแถวเดียว,
        /// กรณีอื่นๆ คื่นค่า <b>Null</b> หรือ default
        /// </returns>
        public abstract T GetData(string id);

        public abstract List<T> GetDatas(string id);

        public virtual IQueryable<T> GetAll()
        {
            IQueryable<T> query = DbContext.Set<T>();
            return query;
        }

        public IQueryable<T> FindBy(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {
            IQueryable<T> query = DbContext.Set<T>().Where(predicate);
            return query;
        }

        /// <summary>
        /// การจัดการข้อมูลในฐานข้อมูล(insert, update, delete) ต้องทำผ่าน method นี้ และต้องมี id, data, action และ resultStatus ด้วย
        /// </summary>
        /// <param name="id">
        /// id เป็นรหัสของแถวหรือเป็นคีย์หลัก
        /// </param>
        /// <param name="data">
        /// ข้อมูลที่จะบันทึกเข้าสู่ฐานข้อมูล หรือข้อมูลที่จะลบออกจากฐานข้อมูล
        /// </param>
        /// <param name="action">
        /// ประเภทของการจัดการข้อมูลในฐานข้อมูล
        /// - Add
        /// - Edit
        /// - Delete
        /// </param>
        /// <param name="resultStatus">
        /// สถานะการจัดการข้อมูล 
        /// - กำหนดค่า <b>True</b> กรณีทำรายการสำเร็จ
        /// - กำหนดค่า <b>False</b> กรณีอื่นๆ
        /// </param>
        /// <returns></returns>
        private void Save(string id, T data, ActionType action, out bool resultStatus)
        {
            resultStatus = false;
            try
            {
                switch (action)
                {
                    case ActionType.Add:
                        this.dbContext.Set<T>().Add(data);
                        break;
                    case ActionType.Edit:
                        this.dbContext.Entry(data).State = EntityState.Modified;
                        break;
                    case ActionType.Delete:
                        this.dbContext.Set<T>().Remove(data);
                        break;
                }
                resultStatus = true;
            }
            catch (Exception ex)
            {
                throw new Exception(GetEntityErrorException(ex));
            }
        }
        public virtual void SaveBulk(List<T> dataList,int commitCount, ActionType actionType, bool recreateContext)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                RRSmartAgentModelContext context = this.DbContext;
                try
                {
                    context.Configuration.AutoDetectChangesEnabled = false;
                    int count = 0;

                    foreach (var data in dataList)
                    {
                        count++;
                        context = SaveToContext(context, data, count, commitCount, actionType, recreateContext);
                    }

                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw new Exception(GetEntityErrorException(ex));
                }
                finally
                {
                    if (context != null)
                        context.Dispose();
                }

                scope.Complete();
            }
        }

        private RRSmartAgentModelContext SaveToContext(RRSmartAgentModelContext context, T data, int count, int commitCount,ActionType actionType, bool recreateContext)
        {
            if (actionType == ActionType.Add)
                context.Set<T>().Add(data);
            else
                context.Entry(data).State = EntityState.Modified;

            if (count % commitCount == 0)
            {
                context.SaveChanges();
                if (recreateContext)
                {
                    context.Dispose();
                    context = new RRSmartAgentModelContext();
                    context.Configuration.AutoDetectChangesEnabled = false;
                }
            }

            return context;
        }

        public virtual T BindObjectToDB(object input)
        {
            T data = new T();

            NameValueCollection form = new NameValueCollection();

            input.GetType().GetProperties().ToList()
                .ForEach(pi => form.Add(pi.Name, pi.GetValue(input, null) == null ? "" : pi.GetValue(input, null).ToString()));

            var props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (var prop in props)
            {
                var propType = prop.PropertyType;
                if (propType.IsGenericType && propType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
                    propType = new NullableConverter(propType).UnderlyingType;

                object value = null;
                string requestData = form[prop.Name];
                if (requestData != null)
                {
                    if (!string.IsNullOrEmpty(requestData))
                        value = requestData;
                }
                else
                {
                    requestData = form[prop.Name.ToPascalFormat().ToLowerFirstChar()];
                    if (requestData != null)
                    {
                        if (!string.IsNullOrEmpty(requestData))
                            value = requestData;
                    }
                }

                if (prop.CanWrite && value != null)
                {
                    PropertyInfo propInfo = data.GetType().UnderlyingSystemType.GetProperty(prop.Name);
                    if (propInfo != null)
                    {
                        if (propType.IsEnum)
                        {
                            try
                            {
                                value = System.Enum.Parse(propType, value.ToString());
                            }
                            catch
                            {
                                value = System.Enum.GetName(propType, 0);
                                value = System.Enum.Parse(propType, value.ToString());
                            }
                        }
                        else if (propType.Equals(typeof(Byte[])))
                        {
                            value = Convert.FromBase64String(value.ToString());
                        }
                        else if (propType.Equals(typeof(Boolean)))
                        {
                            value = value.ToString().ToBoolean();
                        }

                        if (propType.Equals(typeof(Guid)))
                        {
                            Guid guid = new Guid(value.ToString());
                            propInfo.SetValue(data, Convert.ChangeType(guid, propType), null);
                        }
                        else
                        {
                            propInfo.SetValue(data, Convert.ChangeType(value, propType), null);
                        }
                    }
                }
            }

            return data;
        }

        public virtual T BindDBToDB(object input)
        {
            T data = new T();

            NameValueCollection form = new NameValueCollection();

            input.GetType().GetProperties().ToList()
                .ForEach(pi => form.Add(pi.Name, pi.GetValue(input, null) == null ? "" : pi.GetValue(input, null).ToString()));

            var props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (var prop in props)
            {
                var propType = prop.PropertyType;
                if (propType.IsGenericType && propType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
                    propType = new NullableConverter(propType).UnderlyingType;

                object value = null;
                string requestData = form[prop.Name];
                if (requestData != null)
                {
                    if (!string.IsNullOrEmpty(requestData))
                        value = requestData;
                }
                else
                {
                    requestData = form[prop.Name];
                    if (requestData != null)
                    {
                        if (!string.IsNullOrEmpty(requestData))
                            value = requestData;
                    }
                }

                if (prop.CanWrite && value != null)
                {
                    PropertyInfo propInfo = data.GetType().UnderlyingSystemType.GetProperty(prop.Name);
                    if (propInfo != null)
                    {
                        if (propType.IsEnum)
                        {
                            try
                            {
                                value = System.Enum.Parse(propType, value.ToString());
                            }
                            catch
                            {
                                value = System.Enum.GetName(propType, 0);
                                value = System.Enum.Parse(propType, value.ToString());
                            }
                        }
                        else if (propType.Equals(typeof(Byte[])))
                        {
                            value = Convert.FromBase64String(value.ToString());
                        }
                        else if (propType.Equals(typeof(Boolean)))
                        {
                            value = value.ToString().ToBoolean();
                        }

                        if (propType.Equals(typeof(Guid)))
                        {
                            Guid guid = new Guid(value.ToString());
                            propInfo.SetValue(data, Convert.ChangeType(guid, propType), null);
                        }
                        else
                        {
                            propInfo.SetValue(data, Convert.ChangeType(value, propType), null);
                        }
                    }
                }
            }

            return data;
        }

        public virtual object BindDBToObject(object input)
        {
            object data = new object();
            NameValueCollection form = new NameValueCollection();

            input.GetType().GetProperties().ToList()
                .ForEach(pi => form.Add(pi.Name, pi.GetValue(input, null) == null ? "" : pi.GetValue(input, null).ToString()));

            var props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (var prop in props)
            {
                var propType = prop.PropertyType;
                if (propType.IsGenericType && propType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
                    propType = new NullableConverter(propType).UnderlyingType;

                object value = null;
                string requestData = form[prop.Name];
                if (requestData != null)
                {
                    if (!string.IsNullOrEmpty(requestData))
                        value = requestData;
                }
                else
                {
                    requestData = form[prop.Name.ToPascalFormat().ToLowerFirstChar()];
                    if (requestData != null)
                    {
                        if (!string.IsNullOrEmpty(requestData))
                            value = requestData;
                    }
                }

                if (prop.CanWrite && value != null)
                {
                    PropertyInfo propInfo = data.GetType().UnderlyingSystemType.GetProperty(prop.Name);
                    if (propInfo != null)
                    {
                        if (propType.IsEnum)
                        {
                            try
                            {
                                value = System.Enum.Parse(propType, value.ToString());
                            }
                            catch
                            {
                                value = System.Enum.GetName(propType, 0);
                                value = System.Enum.Parse(propType, value.ToString());
                            }
                        }
                        else if (propType.Equals(typeof(Byte[])))
                        {
                            value = Convert.FromBase64String(value.ToString());
                        }
                        else if (propType.Equals(typeof(Boolean)))
                        {
                            value = value.ToString().ToBoolean();
                        }

                        if (propType.Equals(typeof(Guid)))
                        {
                            Guid guid = new Guid(value.ToString());
                            propInfo.SetValue(data, Convert.ChangeType(guid, propType), null);
                        }
                        else
                        {
                            propInfo.SetValue(data, Convert.ChangeType(value, propType), null);
                        }
                    }
                }
            }

            return data;
        }
    }
}
