﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRApiFramework.Utility.Pagination
{
    public class PageManager
    {
        public List<object> getResultList(List<object> list, int inputLimitPerPage, int inputCurrentPage)
        {
            List<object> listResponse = new List<object>();
            PaginationResponse page = new PaginationResponse();
            if (inputCurrentPage == 1)
                listResponse = list.Take(inputLimitPerPage).ToList();
            else
                listResponse = list.Skip(((inputCurrentPage - 1) * inputLimitPerPage))
                    .Take(inputLimitPerPage).ToList();

            return listResponse;
        }

        public PaginationResponse getPagination(int countResult, double inputLimitPerPage, int inputcurrentPage)
        {
            PaginationResponse _pagination = new PaginationResponse();
            var total = countResult;
            var pageSize = (int)Math.Ceiling((double)total / inputLimitPerPage);
            _pagination.currentPage = (inputcurrentPage < 0 ? 0 : inputcurrentPage);

            if (inputcurrentPage == pageSize)
                _pagination.nextPage = pageSize;
            else
                _pagination.nextPage = pageSize == 1 ? 1 : inputcurrentPage + 1;

            if (inputcurrentPage == 1)
                _pagination.prevPage = 1;
            else
                _pagination.prevPage = pageSize == 1 ? 1 : inputcurrentPage - 1;

            return _pagination;


        }

        public PageInfoReponse getPageInfo(int countResult, int countSummary)
        {
            PageInfoReponse _pageinfo = new PageInfoReponse();

            _pageinfo.totalResults = countSummary;
            _pageinfo.resultsPerPage = countResult;

            return _pageinfo;

        }
    }
}