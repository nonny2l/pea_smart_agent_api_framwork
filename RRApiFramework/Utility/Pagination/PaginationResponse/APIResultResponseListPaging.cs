﻿
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RRApiFramework.HTTP.Response;
using RRApiFramework.Model.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Web.Http.ModelBinding;

namespace RRApiFramework.Utility.Pagination
{
    public class APIResultResponseListPaging
    {
        [Required]
        [Description("สถานะข้อมูล (true,false)")]
        public bool success { get; set; }

        [Required]
        [Description("รหัสสถานะข้อมูล")]
        public string code { get; set; }

        [Required]
        [Description("ข้อความตอบกลับจากระบบ ")]
        public string message { get; set; }

        [Required]
        [Description("รายละเอียด ")]
        public string description { get; set; }

        [Required]
        [Description("หมายเลขการรับส่งข้อมูล ")]
        public string transactionId { get; set; }

        [Required]
        [Description("เวลาการรับส่งข้อมูล ")]
        public string transactionDateTime { get; set; }

        [Description("เนื้อหาข้อมูลที่ต้องการ ")]
        public dynamic items { get; set; }

        [Description("เนื้อหาข้อมูลอื่นๆที่ต้องการ ")]
        public dynamic otherItems { get; set; }

        [Description("เนื้อหาการกรองข้อมูลที่ต้องการ ")]
        public dynamic filter { get; set; }

        [Required]
        [Description("ชุดข้อมูลการจัดการ หน้าของเนื่อหาข้อมูล ")]
        public PaginationResponse pagination { get; set; }

        [Required]
        [Description("ชุดข้อมูลแสดงหน้าของเนื่อหาข้อมูล ")]
        public PageInfoReponse pageInfo { get; set; }

        [Required]
        [Description("รหัสความปลอดภัยผู้ใช้งาน")]
        public string key { get; set; }

        private PageManager pageMaster = new PageManager();

        public APIResultResponseListPaging()
        {
            code = "200";
            message = "Success";
            description = "";
            pagination = new PaginationResponse();
            pageInfo = new PageInfoReponse();
            transactionId = Guid.NewGuid().ToString();
            transactionDateTime = DateTime.Now.ToString();

        }


        public HttpResponseMessage ResponseResult(HttpRequestMessage request, List<object> itemListResponse, ModelStateDictionary modelState, PaginationRequest pagination, object model, string ModuleName, string methodName)
        {
            HttpResponseMessage response;
            if (modelState.IsValid)
            {
                this.success = true;
                var itemList = pageMaster.getResultList(itemListResponse, pagination.limitPerPage, pagination.currentPage);
                this.items = itemList;
                this.pagination = pageMaster.getPagination(itemListResponse.Count(), pagination.limitPerPage, pagination.currentPage);
                this.pageInfo = pageMaster.getPageInfo(itemList.Count(), itemListResponse.Count());

                response = HttpManageResponse.Response.ResponseMessage(request, StatusCode.OK, StatusCode.OK.Value(), this);
                //Save_log.SaveLogObject(this, JObject.Parse(JsonConvert.SerializeObject(model)), ModuleName, methodName);//save log output

            }
            else
            {

                var erorMessage = ErrorMessageManagement.GetRequestModelErrorIsValid(modelState);
                response = HttpManageResponse.Response.ResponseMessage(request, StatusCode.OK, StatusCode.OK.Value(), erorMessage);
                //Save_log.SaveLogObject(this, JObject.Parse(JsonConvert.SerializeObject(model)), ModuleName, methodName);

            }
            return response;
        }

    }

    public class FilterItem
    {
        public string value { get; set; }
        public string text { get; set; }
    }
}