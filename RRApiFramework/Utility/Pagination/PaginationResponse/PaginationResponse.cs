﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRApiFramework.Utility.Pagination
{
    public class PaginationResponse
    {
        public int currentPage { get; set; }
        public int nextPage { get; set; }
        public int prevPage { get; set; }


        public PaginationResponse()
        {

            currentPage = 0;
            nextPage = 0;
            prevPage = 0;

        }
    }
}