﻿using System.Web.Configuration;

namespace RRApiFramework.Utility
{
    public class ConfigurationUtil
    {
        private static ConfigurationUtil util = new ConfigurationUtil();
        private string sysTemName;
        private string enableRequestObjectOther;
        private string tokenSecretKey;
        private string tokenExpireUnit;
        private int tokenExpireQty;
        private string enableExpireToken;
        private string[] linkAllow;
        private string modelName;
        private string modelConnectionStringSever;
        private string modelConnectionStringUser;
        private string modelConnectionStringPassword;
        private string modelProviderName;
        private string initCrypto;

        public ConfigurationUtil()
        {

            this.sysTemName = WebConfigurationManager.AppSettings["SYSTEM_NAME"].ToString();
            this.enableRequestObjectOther = WebConfigurationManager.AppSettings["EnableRequestObjectOther"].ToString();
            this.tokenSecretKey = WebConfigurationManager.AppSettings["TOKEN_SECRET_KEY"].ToString();
            this.tokenExpireUnit = WebConfigurationManager.AppSettings["TIME_EXPIRE_UNIT"].ToString();
            this.tokenExpireQty = (WebConfigurationManager.AppSettings["TIME_EXPIRE_QTY"].ToString()).ToInt32();
            this.enableExpireToken = WebConfigurationManager.AppSettings["ENABLE_EXPIRE_TOKEN"].ToString();
            string liinkAllowString = WebConfigurationManager.AppSettings["LINK_ALLOW"].ToString();
            this.linkAllow = liinkAllowString.Split(',');
            this.modelName = WebConfigurationManager.AppSettings["MODEL_NAME"].ToString();
            this.modelConnectionStringSever = WebConfigurationManager.AppSettings["MODEL_CONNECTION_STRING_SERVER"].ToString();
            this.modelConnectionStringUser = WebConfigurationManager.AppSettings["MODEL_CONNECTION_STRING_USER"].ToString();
            this.modelConnectionStringPassword = WebConfigurationManager.AppSettings["MODEL_CONNECTION_STRING_PASSWORD"].ToString();
            this.modelProviderName = WebConfigurationManager.AppSettings["MODEL_PROVIDER_NAME"].ToString();
            this.initCrypto = WebConfigurationManager.AppSettings["INIT_CRYPTO"].ToString();

            

        }
        public static ConfigurationUtil Static
        {
            get { return util = new ConfigurationUtil(); }
        }
        public string SysTemName
        {
            get { return this.sysTemName; }
            set { this.sysTemName = value; }
        }
        public string EnableRequestObjectOther
        {
            get { return this.enableRequestObjectOther; }
            set { this.enableRequestObjectOther = value; }
        }
        public string TokenSecretKey
        {
            get { return this.tokenSecretKey; }
            set { this.tokenSecretKey = value; }
        }
        public string TokenExpireUnit
        {
            get { return this.tokenExpireUnit; }
            set { this.tokenExpireUnit = value; }
        }
        public int TokenExpireQty
        {
            get { return this.tokenExpireQty; }
            set { this.tokenExpireQty = value; }
        }
        public string EnableExpireToken
        {
            get { return this.enableExpireToken; }
            set { this.enableExpireToken = value; }
        }
        public string[] LinkAllow
        {
            get { return this.linkAllow; }
            set { this.linkAllow = value; }
        }
        public string ModelName
        {
            get { return this.modelName; }
            set { this.modelName = value; }
        }
        public string ModelConnectionStringServer
        {
            get { return this.modelConnectionStringSever; }
            set { this.modelConnectionStringSever = value; }
        }
        public string ModelConnectionStringUser
        {
            get { return this.modelConnectionStringUser; }
            set { this.modelConnectionStringUser = value; }
        }
        public string ModelConnectionStringPassword
        {
            get { return this.modelConnectionStringPassword; }
            set { this.modelConnectionStringPassword = value; }
        }
        public string ModelProviderName
        {
            get { return this.modelProviderName; }
            set { this.modelProviderName = value; }
        }

        public string InitCrypto
        {
            get { return this.initCrypto; }
            set { this.initCrypto = value; }
        }

        


    }

}