﻿namespace RRApiFramework.Utility
{
    using System.ComponentModel;


    public enum ComponentModelType
    {
        [DefaultValue("Description")]
        Description,

        [DefaultValue("DisplayName")]
        DisplayName,

        [DefaultValue("Required")]
        Required,

        [DefaultValue("MaxLength")]
        MaxLength
    }

    public enum SpecApiDataType
    {
        [DefaultValue("String")]
        String,

        [DefaultValue("List<String>")]
        ListString,

        [DefaultValue("Int")]
        Int,

        [DefaultValue("List<Int>")]
        ListInt,

        [DefaultValue("Boolean")]
        Boolean,

        [DefaultValue("Decimal")]
        Decimal,

        [DefaultValue("DateTime")]
        DateTime,

        [DefaultValue("Object")]
        Object,

        [DefaultValue("List<Object>")]
        ListObject,
    }
    public enum RequireDataType
    {
        [DefaultValue("M")]
        Mandatory,

        [DefaultValue("0")]
        Optional,
    }


}