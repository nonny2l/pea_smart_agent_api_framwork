﻿using System.Collections.Generic;

namespace RRApiFramework.Utility.ExportExel
{
    public class SummaryPaymentRequest : DynamicReportRequest
    {
        public List<DataTransferProperty> reportData { get; set; }
        public List<AgentData> subReportData { get; set; }
        public List<SummaryPaymentData> summaryData { get; set; }
        public OtherData otherData { get; set; }
        public bool isSummary { get; set; }
    }

    public class RewardData
    {
        public string rewardName { get; set; }
        public string countAgent { get; set; }
        public string detail { get; set; }
        public string unit { get; set; }
        public string value { get; set; }
    }
    public class AgentData
    {
        public AgentData()
        {
            invoiceNo = "-";
            status = "-";
        }
        public string AgentName { get; set; }
        public string incQty { get; set; }
        public string invoiceNo { get; set; }
        public string status { get; set; }
        public string valueReward { get; set; }
        public string isChang { get; set; }
        public string pvNo { get; set; }

    }
    public class SummaryPaymentData
    {
        public string title { get; set; }
        public int? row { get; set; }
        public int? col { get; set; }
        public Style style { get; set; }

    }
}