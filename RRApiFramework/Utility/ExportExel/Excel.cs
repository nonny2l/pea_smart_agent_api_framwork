﻿using ClosedXML.Excel;
using System;
using System.Text.RegularExpressions;

namespace RRApiFramework.Utility.ExportExel
{
    public class Excel
    {
        private int firstColomn;
        private string dateFormat = "dd/MM/yyyy";
        public void setHeaderExcel(IXLWorksheet ws, int fontSize = 8, string fontName = "Tahoma")
        {
         
            ws.Range(ws.FirstCellUsed().Address, ws.LastCellUsed().Address).Style.Font.FontSize = fontSize;
            ws.Range(ws.FirstCellUsed().Address, ws.LastCellUsed().Address).Style.Font.FontName = fontName;
            ws.Range(ws.FirstCellUsed().Address, ws.LastCellUsed().Address).Style.Font.Bold = true;

        }

        public void setFooterExcel(IXLWorksheet ws, int fontSize = 8, string fontName = "Tahoma")
        {

            ws.Range(ws.FirstCellUsed().Address, ws.LastCellUsed().Address).Style.Font.FontSize = fontSize;
            ws.Range(ws.FirstCellUsed().Address, ws.LastCellUsed().Address).Style.Font.FontName = fontName;

        }

        /// <summary>
        /// จัดหัวตาราง
        /// </summary>
        /// <param name="ws">Worksheet</param> 
        public void setHeaderTableExcel(IXLWorksheet ws, int fontSize = 8, string fontName = "Tahoma")
        {
            var lastColumnName = ws.LastColumnUsed().ColumnLetter();
            var lastRows = ws.LastRowUsed().RowNumber();
            //this.firstRowName = ws.LastRowUsed().lastColumnName()
            this.firstColomn = lastRows;
            ws.Range(ws.FirstColumn().ColumnLetter() + lastRows + ":" + lastColumnName + lastRows).Style.Fill.BackgroundColor = XLColor.Black;
            ws.Range(ws.FirstColumn().ColumnLetter() + lastRows + ":" + lastColumnName + lastRows).Style.Font.FontColor = XLColor.White;
            ws.Range(ws.FirstColumn().ColumnLetter() + lastRows + ":" + lastColumnName + lastRows).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            ws.Range(ws.FirstColumn().ColumnLetter() + lastRows + ":" + lastColumnName + lastRows).Style.Font.FontSize = fontSize;
            ws.Range(ws.FirstColumn().ColumnLetter() + lastRows + ":" + lastColumnName + lastRows).Style.Font.FontName = fontName;
            ws.Range(ws.FirstColumn().ColumnLetter() + lastRows + ":" + lastColumnName + lastRows).Style.Font.Bold = true;
        }

        /// <summary>
        /// วาดเส้นตาราง
        /// </summary>
        /// <param name="ws">Worksheet</param> 
        public void setFormatExcel(IXLWorksheet ws, int fontSize = 8, string fontName = "Tahoma")
        {
            int rowStart = this.firstColomn == 0 ? ws.LastRowUsed().RowNumber() : this.firstColomn;
            string lastColumnName = ws.LastColumnUsed().ColumnLetter();
            int lastRows = ws.LastRowUsed().RowNumber();
            //ws.FirstColumn().ColumnLetter();
            ws.Range(ws.FirstColumn().ColumnLetter() + rowStart + ":" + lastColumnName + lastRows).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
            ws.Range(ws.FirstColumn().ColumnLetter() + rowStart + ":" + lastColumnName + lastRows).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
            ws.Range(ws.FirstColumn().ColumnLetter() + rowStart + ":" + lastColumnName + lastRows).Style.Font.FontSize = fontSize;
            ws.Range(ws.FirstColumn().ColumnLetter() + rowStart + ":" + lastColumnName + lastRows).Style.Font.FontName = fontName;

            //ใส่วันที่ Export
            lastColumnName = ws.LastColumnUsed().ColumnLetter();
            lastRows = ws.LastRowUsed().RowNumber();
            lastRows = lastRows + 1;
            DateTime dateNow = DateTime.Now;
            //ws.Cell(lastRows, 1).Value = "Export date : " + dateNow.ToString();
            //ws.Cell(lastRows, 1).Style.Font.FontSize = 14;
            //ws.Cell(lastRows, 1).Style.Font.FontName = "Browallia New";
            //ws.Range("A" + lastRows + ":" + lastColumnName + lastRows).Row(1).Merge();
            int lastColumnNumber = ws.LastColumnUsed().ColumnNumber();
            for (int i = 1; i <= lastColumnNumber; i++)
            {
                ws.Column(i).AdjustToContents();
            }
        }
        /// <summary>
        /// แปลงข้อมูลให้เป็นรูปแบบวันที่ + เวลา
        /// </summary>
        /// <param name="ws"></param>
        /// <param name="Row"></param>
        /// <param name="columnIndex"></param>
        /// <param name="value"></param>
        public void RenderDateTimeValue(IXLWorksheet ws, int Row, int columnIndex, string value)
        {
            DateTime date;
            var isDateTime = DateTime.TryParse(value, out date);
            if (isDateTime)
            {
                ws.Cell(Row, columnIndex).Value = date;
                ws.Cell(Row, columnIndex).DataType = XLDataType.DateTime;
            }
        }
        /// <summary>
        /// แปลงข้อมูลให้เป็นรูปแบบวันที่
        /// </summary>
        /// <param name="ws"></param>
        /// <param name="Row"></param>
        /// <param name="columnIndex"></param>
        /// <param name="value"></param>
        public void RenderDateValue(IXLWorksheet ws, int Row, int columnIndex, string value)
        {
            DateTime date;
            var isDateTime = DateTime.TryParse(value, out date);
            if (isDateTime)
            {
                ws.Cell(Row, columnIndex).Value = date.ToString(dateFormat);
                ws.Cell(Row, columnIndex).Style.DateFormat.Format = dateFormat;
                ws.Cell(Row, columnIndex).DataType = XLDataType.DateTime;
            }
        }
        /// <summary>
        /// แปลงข้อมูลให้เป็นรูปแบบตัวเลข
        /// </summary>
        /// <param name="ws"></param>
        /// <param name="Row"></param>
        /// <param name="columnIndex"></param>
        /// <param name="value"></param>
        public void RenderNumericValue(IXLWorksheet ws, int Row, int columnIndex, string value)
        {
            value = Regex.Replace(value, @"[,]", "");
            decimal nummeric;
            var isNumeric = decimal.TryParse(value, out nummeric);
            if (isNumeric)
            {
                var format = getIdFormatNumeric(value);
                ws.Cell(Row, columnIndex).Value = getTypeNumericValue(value, format);
                ws.Cell(Row, columnIndex).Style.NumberFormat.NumberFormatId = format;
                ws.Cell(Row, columnIndex).DataType = XLDataType.Number;
                ws.Cell(Row, columnIndex).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                ws.Cell(Row, columnIndex).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
            }
        }
        /// <summary>
        /// get Id format numeric.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public int getIdFormatNumeric(string value)
        {
            return (value.Split('.')).Length > 1 ? 4 : 3; //"#,##0.00" : "#,##0"
        }
        /// <summary>
        /// get string format numeric
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public string getStringFormatNumeric(string value)
        {
            return (value.Split('.')).Length > 1 ? "#,##0.00" : "#,##0"; //4 : 3;
        }
        /// <summary>
        /// get data type is double or integer by format 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public object getTypeNumericValue(string value, int format)
        {
            return format == 4 ? double.Parse(value) : int.Parse(value);
        }
        public void setMergeCell(IXLWorksheet ws, string cellMerge, bool isMerge)
        {
            if (isMerge && !string.IsNullOrEmpty(cellMerge))
            {
                //var a = ws.Range(cellMerge);
                ws.Range(cellMerge).Merge();
                ws.Range(cellMerge).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                ws.Range(cellMerge).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
            }
        }

        public void setMergeCell(IXLWorksheet ws, CellProperty startCellMerch, Style style)
        {
            if (style.cellMergeQuantity != null)
            {
                CellProperty endCellMerch = new CellProperty();
                endCellMerch.row = startCellMerch.row + (style.cellMergeQuantity.row - 1);
                endCellMerch.col = startCellMerch.col + (style.cellMergeQuantity.col - 1);

                ws.Range(ws.Cell(startCellMerch.row, startCellMerch.col), ws.Cell(endCellMerch.row, endCellMerch.col)).Merge();

                if (!string.IsNullOrWhiteSpace(style.textAlignHorizontal))
                    ws.Range(ws.Cell(startCellMerch.row, startCellMerch.col), ws.Cell(endCellMerch.row, endCellMerch.col)).Style.Alignment.Horizontal = style.textAlignHorizontal.ToUpper() == "R" ? XLAlignmentHorizontalValues.Right : style.textAlignHorizontal.ToUpper() == "C" ? XLAlignmentHorizontalValues.Center : XLAlignmentHorizontalValues.Left;

                if (!string.IsNullOrWhiteSpace(style.textAlignVertical))
                    ws.Range(ws.Cell(startCellMerch.row, startCellMerch.col), ws.Cell(endCellMerch.row, endCellMerch.col)).Style.Alignment.Vertical = style.textAlignVertical.ToUpper() == "T" ? XLAlignmentVerticalValues.Top : style.textAlignVertical.ToUpper() == "C" ? XLAlignmentVerticalValues.Center : XLAlignmentVerticalValues.Bottom;

                if (style.border)
                {
                    ws.Range(ws.Cell(startCellMerch.row, startCellMerch.col), ws.Cell(endCellMerch.row, endCellMerch.col)).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    ws.Range(ws.Cell(startCellMerch.row, startCellMerch.col), ws.Cell(endCellMerch.row, endCellMerch.col)).Style.Border.InsideBorder = XLBorderStyleValues.Thin;

                }

                if (!string.IsNullOrWhiteSpace(style.backgroundColor))
                    ws.Range(ws.Cell(startCellMerch.row, startCellMerch.col), ws.Cell(endCellMerch.row, endCellMerch.col)).Style.Fill.BackgroundColor = XLColor.FromHtml(style.backgroundColor);

                if (!string.IsNullOrWhiteSpace(style.fontColor))
                    ws.Range(ws.Cell(startCellMerch.row, startCellMerch.col), ws.Cell(endCellMerch.row, endCellMerch.col)).Style.Font.FontColor = XLColor.FromHtml(style.fontColor);

                if (style.textRatation != 0)
                    ws.Range(ws.Cell(startCellMerch.row, startCellMerch.col), ws.Cell(endCellMerch.row, endCellMerch.col)).Style.Alignment.TextRotation = style.textRatation;

            }
        }

        public void setTextAlign(IXLWorksheet ws, int row, int col, string textAlignHorizontal, string textAlignVertical)
        {
            ws.Cell(row, col).Style.Alignment.Horizontal = textAlignHorizontal.ToUpper() == "R" ? XLAlignmentHorizontalValues.Right : textAlignHorizontal.ToUpper() == "C" ? XLAlignmentHorizontalValues.Center : XLAlignmentHorizontalValues.Left;
            ws.Cell(row, col).Style.Alignment.Vertical = textAlignVertical.ToUpper() == "T" ? XLAlignmentVerticalValues.Top : textAlignVertical.ToUpper() == "C" ? XLAlignmentVerticalValues.Center : XLAlignmentVerticalValues.Bottom;
        }

        public void setFont(IXLWorksheet ws, int row, int col, bool isBold, int fontSize)
        {
            ws.Cell(row, col).Style.Font.Bold = isBold;
            ws.Cell(row, col).Style.Font.FontSize = fontSize;
        }

        public void setFontUnderLine(IXLWorksheet ws, int row, int col, int underLineId)
        {
            // 0 = Double
            // 2 = None
            // 3 = Single
            ws.Cell(row, col).Style.Font.Underline = underLineId == 1 ? XLFontUnderlineValues.Double : underLineId == 3 ? XLFontUnderlineValues.Single : XLFontUnderlineValues.None;
        }

        public void setTextRotation(IXLWorksheet ws, int row, int col, int textRotation)
        {
            ws.Cell(row, col).Style.Alignment.TextRotation = textRotation;
        }

        public void setShowGridLines(IXLWorksheet ws, bool isShow)
        {
            ws.ShowGridLines = isShow;
        }

        public void setBoderCell(IXLWorksheet ws, int row, int col)
        {
            ws.Cell(row, col).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
            ws.Cell(row, col).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
        }

        public void setBoderCell(IXLWorksheet ws, bool isMerge, string cellMerge)
        {
            ws.Range(cellMerge).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
            ws.Range(cellMerge).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
        }

        public void setBackgroundColor(IXLWorksheet ws, int row, int col, string backgroundColor, string fontColor)
        {
            ws.Cell(row, col).Style.Fill.BackgroundColor = !string.IsNullOrEmpty(backgroundColor) ? XLColor.FromHtml(backgroundColor) : XLColor.NoColor;
            ws.Cell(row, col).Style.Font.FontColor = !string.IsNullOrEmpty(fontColor) ? XLColor.FromHtml(fontColor) : XLColor.Black;
        }

        public void setStyle(IXLWorksheet ws, int row, int col, Style style)
        {
            if (style != null)
            {
                setTextAlign(ws, row, col, style.textAlignHorizontal, style.textAlignVertical);
                setFont(ws, row, col, style.fontBold, style.fontSize);
                if (style.isMerge)
                {
                    CellProperty startCellProperty = new CellProperty();
                    startCellProperty.col = col;
                    startCellProperty.row = row;

                    setMergeCell(ws, startCellProperty, style);
                    //  setBoderCell(ws, style.isMerge, style.cellMerge);
                }
                else
                {
                    if (style.textRatation != 0)
                    {
                        setTextRotation(ws, row, col, style.textRatation);
                    }
                    if (style.border)
                    {
                        setBoderCell(ws, row, col);
                    }
                    if (!string.IsNullOrEmpty(style.backgroundColor) || !string.IsNullOrEmpty(style.fontColor))
                    {
                        setBackgroundColor(ws, row, col, style.backgroundColor, style.fontColor);
                    }
                }
            }

            //if (style.dataType != "S")
            //{
            //    setDataTypeFormat(ws, row, col, style.dataType, value);
            //}
        }

        public void setDataTypeFormat(IXLWorksheet ws, int row, int columnIndex, string dataType, string value)
        {
            if (dataType != "S")
            {
                switch (dataType)
                {
                    case "N":
                        {
                            #region Number format
                            RenderNumericValue(ws, row, columnIndex, value);
                            break;
                            #endregion
                        }
                    case "D":
                        {
                            #region Date Format
                            RenderDateValue(ws, row, columnIndex, value);
                            break;
                            #endregion
                        }
                    case "DT":
                        {
                            #region Date Format
                            RenderDateTimeValue(ws, row, columnIndex, value);
                            break;
                            #endregion
                        }
                    default:
                        {
                            RenderNumericValue(ws, row, columnIndex, value);
                            RenderDateValue(ws, row, columnIndex, value);
                            RenderDateTimeValue(ws, row, columnIndex, value);
                            //ws.Cell(Row, columnIndex).Value = propValue;
                            break;
                        }
                }
            }
            else
            {
                ws.Cell(row, columnIndex).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                //ws.Cell(row, columnIndex).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
            }
        }

        public void setAutoWidthColumn(IXLWorksheet ws)
        {
            int lastColumnNumber = ws.LastColumnUsed().ColumnNumber();
            for (int i = 1; i <= lastColumnNumber; i++)
            {
                ws.Column(i).AdjustToContents();
            }
        }
    }
}