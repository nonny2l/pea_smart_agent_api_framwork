﻿using ClosedXML.Excel;
using System.IO;
using System.Web;

namespace RRApiFramework.Utility.ExportExel
{
    public static class Exports
    {
        public static void Excel(XLWorkbook workbook, string fileName)
        {
            // Create the workbook

            // Prepare the response
            HttpResponse httpResponse = HttpContext.Current.Response;
            httpResponse.Clear();
            httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            httpResponse.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xlsx;", fileName));

            // Flush the workbook to the Response.OutputStream
            using (MemoryStream memoryStream = new MemoryStream())
            {
                workbook.SaveAs(memoryStream);
                memoryStream.WriteTo(httpResponse.OutputStream);
                memoryStream.Close();
            }

            httpResponse.End();
        }
      
    }
}