﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRApiFramework.HTTP.Response
{
    public class ActionResultStatus
    {
        public bool success { get; set; }
        public int code { get; set; }
        public string message { get; set; }
        public string description { get; set; }
        public dynamic data { get; set; }
        public dynamic filter { get; set; }
        public dynamic otherData { get; set; }
        public string key { get; set; }

        public ActionResultStatus()
        {
            success = true;
            code = 200;
            message = "OK";
            key = "";
        }
    }
}
