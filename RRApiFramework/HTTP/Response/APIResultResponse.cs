﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRApiFramework.HTTP.Response
{
    public class APIResultResponse
    {
        [Required]
        [Description("สถานะข้อมูล (true,false)")]
        public bool success { get; set; }

        [Required]
        [Description("รหัสสถานะข้อมูล")]
        public string code { get; set; }

        [Required]
        [Description("ข้อความตอบกลับจากระบบ ")]
        public string message { get; set; }

        [Required]
        [Description("รายละเอียด ")]
        public string description { get; set; }

        [Required]
        [Description("หมายเลขการรับส่งข้อมูล ")]
        public string transactionId { get; set; }

        [Required]
        [Description("เวลาการรับส่งข้อมูล ")]
        public string transactionDateTime { get; set; }


        [Description("เนื้อหาข้อมูลที่ต้องการ ")]
        public dynamic items { get; set; }

        [Description("เนื้อหาข้อมูลอื่นๆที่ต้องการ ")]
        public dynamic otherItems { get; set; }

        [Description("เนื้อหาการกรองข้อมูลที่ต้องการ ")]
        public dynamic filter { get; set; }

        public string key { get; set; }



        public APIResultResponse()
        {
            code = "200";
            message = "Success";
            description = "";
            transactionId = Guid.NewGuid().ToString();
            transactionDateTime = DateTime.Now.ToString();
        }
    }
}
