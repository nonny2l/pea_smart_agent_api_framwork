﻿using JWT;
using JWT.Algorithms;
using JWT.Builder;
using Newtonsoft.Json;
using RRApiFramework.HTTP.Response;
using RRApiFramework.Utility;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace RRApiFramework
{
    public class TokenValidationHandler : DelegatingHandler
    {
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            HttpResponseMessage response;
            ActionResultStatus actionResultStatus = new ActionResultStatus();
            string link = System.Web.HttpContext.Current.Request.RawUrl;
            IJwtAlgorithm algorithm = new HMACSHA256Algorithm();

            if (ConfigurationUtil.Static.LinkAllow.Contains(link))
            {
                return response = await base.SendAsync(request, cancellationToken);
            }
            else
            {

                long utcDateTimeNow = DateTimeOffset.UtcNow.ToUnixTimeSeconds();
                AuthenticationHeaderValue authorization = request.Headers.Authorization;
                if (authorization != null)
                {
                    try
                    {
                        string authParam = authorization.Parameter;
                        IDateTimeProvider provider = new UtcDateTimeProvider();
                        var now = provider.GetNow();

                        var secret = ConfigurationUtil.Static.TokenSecretKey;

                        var jwtDeserialize = new JwtBuilder()
                                   .WithSecret(secret)
                                   .WithAlgorithm(algorithm)
                                   .MustVerifySignature()
                                   .Decode(authParam);

                        UserAccess userAccess = JsonConvert.DeserializeObject<UserAccess>(jwtDeserialize);
                        if (ConfigurationUtil.Static.EnableExpireToken == "Y")
                        {
                            if (utcDateTimeNow > userAccess.tokenDateTime)
                            {
                                actionResultStatus.code = (int)HttpStatusCode.RequestTimeout;
                                actionResultStatus.data = null;
                                actionResultStatus.message = "Token has expired";
                                actionResultStatus.description = null;

                                return response = request.CreateResponse(HttpStatusCode.RequestTimeout, actionResultStatus);
                            }
                        }

                        var userNameClaim = new Claim(ClaimTypes.Name, JsonConvert.SerializeObject(userAccess));
                        var identity = new ClaimsIdentity(new[] { userNameClaim }, authorization.Scheme);
                        var principal = new ClaimsPrincipal(identity);
                        Thread.CurrentPrincipal = principal;
                        if (System.Web.HttpContext.Current != null)
                        {
                            System.Web.HttpContext.Current.User = principal;
                        }

                    }
                    catch (SignatureVerificationException)
                    {
                        actionResultStatus.code = (int)HttpStatusCode.Unauthorized;
                        actionResultStatus.data = null;
                        actionResultStatus.message = "Token has invalid signature";
                        actionResultStatus.description = null;

                        return response = request.CreateResponse(HttpStatusCode.Unauthorized, actionResultStatus);

                    }
                    catch (Exception ex)
                    {
                        actionResultStatus.success = false;
                        actionResultStatus.code = (int)HttpStatusCode.Unauthorized;
                        actionResultStatus.data = null;
                        actionResultStatus.message = "Token has invalid signature";
                        actionResultStatus.description = ex.ErrorException();

                        return response = request.CreateResponse(HttpStatusCode.Unauthorized, actionResultStatus);

                    }
                }
                else
                {
                    actionResultStatus.code = (int)HttpStatusCode.Unauthorized;
                    actionResultStatus.data = null;
                    actionResultStatus.message = "Unauthorized";
                    actionResultStatus.description = null;

                    return response = request.CreateResponse(HttpStatusCode.Unauthorized, actionResultStatus);

                }
                return response = await base.SendAsync(request, cancellationToken);
            }
        }





    }
}