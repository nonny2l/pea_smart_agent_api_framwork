﻿using JWT;
using JWT.Algorithms;
using JWT.Builder;
using JWT.Serializers;
using Newtonsoft.Json;
using RRApiFramework.Utility;
using System;
using System.Web.Configuration;

namespace RRApiFramework.Security
{
    public static class JwtManagement
    {
        public static string GenerateJasonWebToken()
        {
            string result;
            string userName = System.Web.HttpContext.Current.User.Identity.Name;
            try
            {
                var secret = ConfigurationUtil.Static.TokenSecretKey;
                // SECOND , MINUTES , HOURS
                string timeExUnit = ConfigurationUtil.Static.TokenExpireUnit;
                int timeExQty = ConfigurationUtil.Static.TokenExpireQty;
                UserAccess userAccess = new UserAccess();

                switch (timeExUnit)
                {
                    case "SECOND":
                        userAccess.tokenDateTime = DateTimeOffset.UtcNow.AddSeconds(timeExQty).ToUnixTimeSeconds();
                        break;
                    case "MINUTES":
                        userAccess.tokenDateTime = DateTimeOffset.UtcNow.AddMinutes(timeExQty).ToUnixTimeSeconds();
                        break;
                    case "HOURS":
                        userAccess.tokenDateTime = DateTimeOffset.UtcNow.AddHours(timeExQty).ToUnixTimeSeconds();
                        break;

                }
                userAccess.userName = userName;
                var jsonUserAccess = JsonConvert.SerializeObject(userAccess);
                IJwtAlgorithm algorithm = new HMACSHA256Algorithm();
                IJsonSerializer serializer = new JsonNetSerializer();
                IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
                IJwtEncoder encoder = new JwtEncoder(algorithm, serializer, urlEncoder);

                result = encoder.Encode(userAccess, secret);

            }
            catch (Exception e)
            {
                throw new Exception(e.ErrorException());
            }

            return result;
        }

        public static string GenerateJasonWebToken(UserAccess userAccess)
        {
            string result;
            string userName = System.Web.HttpContext.Current.User.Identity.Name;
            try
            {
                if (userAccess == null)
                    userAccess = new UserAccess();

                var secret = ConfigurationUtil.Static.TokenSecretKey;
                // SECOND , MINUTES , HOURS
                string timeExUnit = ConfigurationUtil.Static.TokenExpireUnit;
                int timeExQty = ConfigurationUtil.Static.TokenExpireQty;

                switch (timeExUnit)
                {
                    case "SECOND":
                        userAccess.tokenDateTime = DateTimeOffset.UtcNow.AddSeconds(timeExQty).ToUnixTimeSeconds();
                        break;
                    case "MINUTES":
                        userAccess.tokenDateTime = DateTimeOffset.UtcNow.AddMinutes(timeExQty).ToUnixTimeSeconds();
                        break;
                    case "HOURS":
                        userAccess.tokenDateTime = DateTimeOffset.UtcNow.AddHours(timeExQty).ToUnixTimeSeconds();
                        break;

                }
                var jsonUserAccess = JsonConvert.SerializeObject(userAccess);
                IJwtAlgorithm algorithm = new HMACSHA256Algorithm();
                IJsonSerializer serializer = new JsonNetSerializer();
                IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
                IJwtEncoder encoder = new JwtEncoder(algorithm, serializer, urlEncoder);

                result = encoder.Encode(userAccess, secret);

            }
            catch (Exception e)
            {
                throw new Exception(e.ErrorException());
            }

            return result;
        }



    }
}