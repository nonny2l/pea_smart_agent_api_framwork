﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRApiFramework
{
    public class UserAccess
    {
        public string userId { get; set; }
        public string userName { get; set; }
        public string userGroupId { get; set; }
        public string userTypeId { get; set; }
        public string empNo { get; set; }
        public string email { get; set; }
        public List<string> userIdList { get; set; }
        public string isActive { get; set; }
        public long tokenDateTime { get; set; }

    }
}