﻿using RRApiFramework.Controller;
using RRApiFramework.Model.Enum;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;

namespace RRApiFramework.Model
{
    /// <summary>
    /// คอนโทรลเลอร์หลักสำหรับจัดการข้อมูลของระบบ
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class AbBLRepository 
    {
        public AbBLRepository()
        {
        }

        //~AbBLRepository()
        //{
        //    Dispose();
        //}

        private string moduleName;
        private bool disposed;
        protected string ModuleName
        {
            get { return this.moduleName = "AbBLRepository"; }
            set { this.moduleName = value; }
        }


        public virtual void SaveLogTransection(string key, ActionType action)
        {
            /// ยังไม่ทราบ Table ที่ใช้งาน
            StackFrame stackFrame = new StackFrame(1, true);
            string[] temp = stackFrame.GetFileName().Split('\\');
            string fileClassName = temp.Length > 0 ? temp[temp.Length - 1] : temp[0];
            string functionName = stackFrame.GetMethod().Name;
            int fileLineNumber = stackFrame.GetFileLineNumber();
            int fileColumnNumber = stackFrame.GetFileColumnNumber();
            string className = stackFrame.GetMethod().DeclaringType.Name;

        }
        public virtual void SaveLogErorTransection(string key, ActionType action, Exception exception)
        {

            try
            {
                string resultException = GetEntityErrorException(exception);


                var errorInfo = string.Empty;

                StackFrame stackFrame = new StackFrame(1, true);
                string[] temp = stackFrame.GetFileName().Split('\\');
                string fileClassName = temp.Length > 0 ? temp[temp.Length - 1] : temp[0];
                string functionName = stackFrame.GetMethod().Name;
                int fileLineNumber = stackFrame.GetFileLineNumber();
                int fileColumnNumber = stackFrame.GetFileColumnNumber();
                string className = stackFrame.GetMethod().DeclaringType.Name;
                string[] stack = exception.StackTrace.Split('\n');

                errorInfo += "///* Comment *///";
                errorInfo += Environment.NewLine;
                errorInfo += "File Name : " + fileClassName + Environment.NewLine;
                errorInfo += "Class Name : " + className + Environment.NewLine;
                errorInfo += "Method Name : " + functionName + Environment.NewLine;
                errorInfo += "Line Number : " + fileLineNumber + Environment.NewLine;
                errorInfo += "Column Number : " + fileColumnNumber + Environment.NewLine;
                errorInfo += "////////////////";

            }
            catch (Exception ex)
            {
                var err = GetEntityErrorException(ex);
            }
        }


        private static string GetEntityErrorException(Exception exception)
        {
            string resultException = string.Empty;

            var entityException = exception.GetType();
            if (entityException == typeof(DbEntityValidationException))
            {
                resultException += EntityErrorException(exception);
            }
            else
            {
                if (exception.InnerException != null)
                {
                    if (exception.InnerException.InnerException != null)
                    {
                        if (exception.InnerException.InnerException.InnerException != null)
                            resultException += exception.InnerException.InnerException.InnerException.Message + Environment.NewLine;
                        else
                            resultException += exception.InnerException.InnerException.Message + Environment.NewLine;
                    }
                    else
                    {
                        resultException += exception.InnerException.Message + Environment.NewLine;
                    }
                }
                else
                {
                    resultException += exception.Message + Environment.NewLine;
                }
            }
            return resultException;
        }
        private static string EntityErrorException(Exception exception)
        {
            string result = string.Empty;
            var errorResult = string.Empty;
            var counts = 1;
            var entityErrors = (DbEntityValidationException)exception;
            foreach (var error in entityErrors.EntityValidationErrors)
            {
                foreach (var err in error.ValidationErrors)
                {
                    if (errorResult.Length > 0)
                        errorResult += Environment.NewLine;

                    errorResult += string.Format("{0}. Error Message : {2}",
                        counts, err.PropertyName, err.ErrorMessage);
                    counts++;
                }
            }

            return errorResult + Environment.NewLine;
        }
       
        //public void Dispose()
        //{
        //    Dispose(true);
        //    GC.SuppressFinalize(this);
        //}

        // Protected implementation of Dispose pattern.
        //protected virtual void Dispose(bool disposing)
        //{
        //    if (!disposed)
        //    {
        //        if (disposing)
        //        {
        //            this.Dispose();
        //        }
        //    }
        //    disposed = true;
        //}
    }
}
